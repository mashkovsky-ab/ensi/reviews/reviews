<?php

namespace App\Http\ApiV1\Modules\Foos\Controllers;

class FoosController
{
    public function get()
    {
        return ['a' => 'b'];
    }
}
